

function slickSlider() {
	$('.slick').slick({
		fade: true,
		autoplay: true,
  });
}

function testemonialSlide() {
	$('.testemonials').slick({
		fade: true,
		autoplay: true,
		arrows: false,
		autoplaySpeed: 3000,
  });
}
$(document).ready(function() {
	slickSlider();
	testemonialSlide();

});

/*$('.left').click(function(){
  $('.slick').slick('slickPrev');
})

$('.right').click(function(){
  $('.slick').slick('slickNext');
})*/


